import { filter, includes, isEmpty, sortBy } from "lodash";

// Store data into Localstorage
export const setUser = (data) => {
	localStorage.setItem("EBUTLER_USER", JSON.stringify(data));
};

// Get Data from Localstorage
export const getUser = () => JSON.parse(localStorage.getItem("EBUTLER_USER"));

// Clear Localstorage
export const clearLS = () => {
	localStorage.clear();
};

// Ṣort alphabetically
export const sortAlpha = (list = []) => (!isEmpty(list) ? sortBy(list, ["name"]) : []);

// Search by name
export const searchByName = (searchingList = [], searchKey) =>
	!isEmpty(searchingList)
		? filter(searchingList, (person) => includes(person.name.toLowerCase(), searchKey.toLowerCase()))
		: [];
