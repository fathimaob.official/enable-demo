import { AppRoutes } from "./routers";

const App = () => <AppRoutes />;

export default App;
