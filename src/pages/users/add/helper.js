// import { uniqueId } from "lodash";
import uniqid from "uniqid";
import { email, required } from "../../../util/formValidator";

// Initial values to user form
export const userInitialValue = {
	name: "",
	email: "",
	address: {
		street: "",
		suite: "",
		city: "",
		zipcode: "",
	},
	company: {
		name: "",
	},
};

// Formik User fields validation
export const validateUserForm = (values) => {
	const errors = {};

	const nameError = required(values.name);
	if (nameError) {
		errors.name = nameError;
	}

	const emailError = required(values.email) ? required(values.email) : email(values.email);
	if (emailError) {
		errors.email = emailError;
	}

	const cityError = required(values.address.city);
	if (cityError) {
		errors.city = cityError;
	}

	const companyError = required(values.company.name);
	if (companyError) {
		errors.companyName = companyError;
	}
	return errors;
};

// User Form Fields to render in formik
export const userFormFields = (handleChange, handleBlur, values, touched, errors) => [
	{
		label: "Name *",
		id: "name",
		name: "name",
		onChange: handleChange,
		onBlur: handleBlur,
		value: values.name,
		helperText: errors && touched && touched.name ? errors.name : null,
		error: Boolean(errors && errors.name && touched && touched.name),
		breakpoints: {
			xs: 12,
		},
	},
	{
		label: "Email *",
		id: "email",
		name: "email",
		onChange: handleChange,
		onBlur: handleBlur,
		value: values.email,
		helperText: errors && touched && touched.email ? errors.email : null,
		error: Boolean(errors && errors.email && touched && touched.email),
		breakpoints: {
			xs: 12,
		},
	},
	{
		label: "Street",
		id: "address.street",
		name: "address.street",
		onChange: handleChange,
		onBlur: handleBlur,
		value: values.address.street,
		breakpoints: {
			xs: 12,
			md: 6,
		},
	},
	{
		label: "Suite",
		id: "address.suite",
		name: "address.suite",
		onChange: handleChange,
		onBlur: handleBlur,
		value: values.address.suite,
		breakpoints: {
			xs: 12,
			md: 6,
		},
	},
	{
		label: "City *",
		id: "address.city",
		name: "address.city",
		onChange: handleChange,
		onBlur: handleBlur,
		value: values.address.city,
		helperText: errors && touched && touched.address && touched.address.city ? errors.city : null,
		error: Boolean(errors && errors.city && touched && touched.address && touched.address.city),
		breakpoints: {
			xs: 12,
			md: 6,
		},
	},
	{
		label: "Zipcode",
		id: "address.zipcode",
		name: "address.zipcode",
		onChange: handleChange,
		onBlur: handleBlur,
		value: values.address.zipcode,
		breakpoints: {
			xs: 12,
			md: 6,
		},
	},
	{
		label: "Company Name *",
		id: "company.name",
		name: "company.name",
		onChange: handleChange,
		onBlur: handleBlur,
		value: values.company.name,
		helperText: errors && touched && touched.company && touched.company.name ? errors.companyName : null,
		error: Boolean(errors && errors.companyName && touched && touched.company && touched.company.name),
		breakpoints: {
			xs: 12,
		},
	},
];

// User ID generated for each user
export const restructureUserBody = (values) => ({
	id: uniqid(),
	...values,
});
