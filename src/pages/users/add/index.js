import React from "react";
import { Button, Drawer, FormControl, Grid, Typography } from "@material-ui/core";
import { Field, Form, Formik } from "formik";
import clsx from "clsx";
import { useDispatch } from "react-redux";
import { restructureUserBody, userFormFields, userInitialValue, validateUserForm } from "./helper";
import { Success, TextInput } from "../../../components";
import { addUser, editUser } from "../../../redux/actions/usersActions";
import { useStyles } from "../styles";

const AddUser = ({ initialValues = null, isOpen, onClose }) => {
	const classes = useStyles();
	const dispatch = useDispatch();

	const isEdit = initialValues ? true : false;

	// Add/Edit User
	const onFormSubmit = (values) => {
		const body = restructureUserBody(values);
		isEdit ? dispatch(editUser(body)) : dispatch(addUser(body));
		Success(isEdit ? "User updated successfully" : "User created successfully!");
		onClose();
	};

	return (
		<Drawer anchor="right" open={isOpen} onClose={onClose} className={classes.addUserDrawer}>
			<div className={classes.addUserWrapper}>
				<Typography className={classes.addUserTitle} variant="h5" gutterBottom>
					{isEdit ? "Edit" : "Add"} User Details
				</Typography>
				<Formik
					initialValues={initialValues ? initialValues : userInitialValue}
					validate={validateUserForm}
					onSubmit={(values, { resetForm, setSubmitting }) => onFormSubmit(values, resetForm, setSubmitting)}>
					{({ handleChange, handleBlur, touched, errors, values, handleSubmit }) => (
						<Form onSubmit={handleSubmit} autoComplete="off">
							{/* Form */}
							<Grid container spacing={3}>
								{userFormFields(handleChange, handleBlur, values, touched, errors).map((item, index) => (
									<Grid key={index} item {...item.breakpoints}>
										<FormControl className={classes.formControl} variant="outlined">
											<Field component={TextInput} {...item} />
										</FormControl>
									</Grid>
								))}
							</Grid>

							{/* Footer  */}
							<div className={classes.addUserFooter}>
								<Button className={classes.btn} onClick={onClose}>
									Cancel
								</Button>
								<Button className={clsx(classes.btn, classes.btnPrimary)} variant="contained" type="submit">
									{isEdit ? "Update" : "Create"}
								</Button>
							</div>
						</Form>
					)}
				</Formik>
			</div>
		</Drawer>
	);
};

export default AddUser;
