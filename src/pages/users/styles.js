import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
	usersHeader: {
		display: "flex",
		marginBottom: "1.5rem",
		width: "50%",
		justifyContent: "center",
	},
	usersIconWrap: {
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#FFF",
		color: "#AA98A9",
		border: "2px solid #AA98A9",
		borderRadius: "50%",
		height: "40px",
		width: "40px",
		marginLeft: "1rem",
		boxShadow: "0px 0px 9px -2px #aa98a9",
		"& img": {
			width: "23px",
			height: "23px",
		},
		"&:hover": {
			backgroundColor: "rgb(170 152 169 / 10%)",
			cursor: "pointer",
		},
	},
	usersTableLinkBtn: {
		padding: 0,
		marginRight: "1rem",
		textTransform: "capitalize",
		minWidth: "0",
		textDecoration: "underline",
	},
	blueLink: {
		color: "#4682B4",
	},
	redLink: {
		color: "#C41E3A",
	},

	// ADD USER
	addUserDrawer: {
		"& .MuiDrawer-paper": {
			borderTopLeftRadius: "3rem",
			borderBottomLeftRadius: "3rem",
			width: "30rem",
		},
	},
	addUserWrapper: {
		padding: "2rem",
	},
	addUserTitle: {
		fontWeight: "bold",
		marginBottom: "1rem",
	},
	formControl: {
		width: "100%",
	},
	addUserFooter: {
		marginTop: "2rem",
		display: "flex",
		justifyContent: "flex-end",
	},
	btn: {
		textTransform: "capitalize",
		marginLeft: "1rem",
	},
	btnSecondary: { color: "#6c616b" },
	btnPrimary: {
		backgroundColor: "#AA98A9",
		color: "#FFF",
	},
}));
