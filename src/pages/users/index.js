import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Layout, Success } from "../../components";
import { UserHeader } from "./components";
import ListUser from "./list";
import { addUsersBulk, getUsersList, searchUsers } from "../../redux/actions";
import AddUser from "./add";
import { restructureUserBody } from "./add/helper";
import { map } from "lodash";

const Users = () => {
	const dispatch = useDispatch();
	const { searchKey } = useSelector((state) => state.Users);

	const [openAddModal, setOpenAddModal] = useState(false);
	const [userToEdit, setUserToEdit] = useState(null);

	// Search Users by NAME
	const doSearch = (e) => dispatch(searchUsers(e));

	// Add user (modal handling)
	const handleAddUserModal = () => {
		setUserToEdit(null);
		setOpenAddModal(true);
	};
	const closeAddUserModal = () => {
		setUserToEdit(null);
		setOpenAddModal(false);
	};

	// Add user bulk
	const doBulkAddUser = (users) => {
		const usersList = map(users, (o) => restructureUserBody(o));
		dispatch(addUsersBulk(usersList));
		Success("Users added successfully");
	};

	// Edit user
	const handleEditUserModal = (row) => {
		setUserToEdit(row);
		setOpenAddModal(true);
	};

	// Load users list on page load
	useEffect(() => {
		dispatch(getUsersList());
	}, [dispatch]);

	return (
		<Layout>
			<UserHeader searchKey={searchKey} doSearch={doSearch} doAddUser={handleAddUserModal} doAddUsers={doBulkAddUser} />
			<ListUser onClickEdit={handleEditUserModal} />
			<AddUser initialValues={userToEdit} isOpen={openAddModal} onClose={closeAddUserModal} />
		</Layout>
	);
};

export default Users;
