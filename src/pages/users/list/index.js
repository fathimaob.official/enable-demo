import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { ConfirmDialog, Error, Success, Tables } from "../../../components";
import { columns } from "./helper.js";
import { useStyles } from "../styles.js";
import { deleteUser } from "../../../redux/actions/usersActions.js";

const ListUser = ({ onClickEdit }) => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const { usersList } = useSelector((state) => state.Users);

	const [isOpenConfirm, setIsOpenConfirm] = useState(false);
	const [deletingUser, setDeletingUser] = useState(null);

	// Open Delete confirmation modal
	const onClickDelete = (row) => {
		if (row && row.id) {
			setIsOpenConfirm(true);
			setDeletingUser(row.id);
		} else Error("User details not found. Try again.");
	};

	// Delete User
	const doDelete = () => {
		deletingUser && dispatch(deleteUser(deletingUser));
		setDeletingUser(null);
		setIsOpenConfirm(false);
		Success("User deleted successfully");
	};

	// Close delete confirmation modal
	const closeConfirmationModal = () => {
		setDeletingUser(null);
		setIsOpenConfirm(false);
	};

	// List : Columns to render in table
	const column = columns(classes, onClickDelete, onClickEdit);

	return (
		<>
			<Tables columns={column} data={usersList} />
			<ConfirmDialog isOpen={isOpenConfirm} onClose={closeConfirmationModal} onYes={doDelete} />
		</>
	);
};

export default ListUser;
