import React from "react";
import clsx from "clsx";
import Button from "@material-ui/core/Button";

export const columns = (classes, doDelete, doEdit) => [
	{ label: "Name", accessor: "name" },
	{ label: "Email", accessor: "email" },
	{
		label: "Actions",
		accessor: "",
		render: (data) => (
			<>
				<Button className={clsx(classes.usersTableLinkBtn, classes.blueLink)} onClick={() => doEdit(data)}>
					Edit
				</Button>
				<Button className={clsx(classes.usersTableLinkBtn, classes.redLink)} onClick={() => doDelete(data)}>
					Delete
				</Button>
			</>
		),
	},
];
