import React, { useState } from "react";
import Tooltip from "@material-ui/core/Tooltip";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import { ExcelImport, Search } from "../../components";
import uploadIcon from "../../assets/images/excel_import.png";
import { useStyles } from "./styles";

export const UserHeader = ({ searchKey, doSearch, doAddUser, doAddUsers }) => {
	const classes = useStyles();

	// Upload modal handling
	const [isOpenUploadModal, setIsOpenUploadModal] = useState(false);
	const handleUploadClick = () => setIsOpenUploadModal(true);

	return (
		<div className={classes.usersHeader}>
			<Search searchKey={searchKey} doSearch={doSearch} />
			<Tooltip title="Add User" arrow>
				<div className={classes.usersIconWrap} onClick={doAddUser}>
					<PersonAddIcon />
				</div>
			</Tooltip>
			<Tooltip title="Import from Excel (xlsx)" arrow>
				<div className={classes.usersIconWrap} onClick={handleUploadClick}>
					<img src={uploadIcon} alt="Excel_Upload" />
				</div>
			</Tooltip>

			<ExcelImport isOpen={isOpenUploadModal} handleClose={() => setIsOpenUploadModal(false)} addUsers={doAddUsers} />
		</div>
	);
};
