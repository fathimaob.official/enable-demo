import React from "react";
import { makeStyles, TextField } from "@material-ui/core";

/**
 * Text Input
 * (Formik Supported)
 */
const TextInput = (props) => {
	const classes = useStyles();
	return <TextField variant="outlined" classes={{ root: classes.formInput }} {...props} />;
};

export const useStyles = makeStyles((theme) => ({
	formInput: {
		"& .MuiFormLabel-root": { color: "#AA98A9" },
		"& .MuiFormLabel-root.Mui-error": { color: "#f44336" },
		"& .MuiOutlinedInput-root:hover .MuiOutlinedInput-notchedOutline": { borderColor: "#AA98A9" },
		"& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline": { borderColor: "#AA98A9" },
		"& .MuiOutlinedInput-root.Mui-error:hover .MuiOutlinedInput-notchedOutline": { borderColor: "#f44336" },
		"& .MuiOutlinedInput-root.Mui-focused.Mui-error .MuiOutlinedInput-notchedOutline": { borderColor: "#f44336" },
		borderColor: "#AA98A9",
	},
}));

export default TextInput;
