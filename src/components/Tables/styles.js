import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
	tableContainer: {
		border: "2px solid #AA98A9",
		borderRadius: "10px",
		height: "80%",
		width: "90%",
		overflowX: "hidden",
		overflowY: "scroll",
		"&::-webkit-scrollbar": {
			width: "8px",
			borderRadius: "20px",
		},
		"&::-webkit-scrollbar-thumb": {
			background: "#AA98A9",
			borderRadius: "20px",
		},
		"&::-webkit-scrollbar-thumb:hover": {
			background: "#b6b8bb",
			borderRadius: "20px",
		},
	},
	table: {
		"& .MuiTableRow-head": {
			backgroundColor: "#ead9e9",
			borderBottom: "2px solid #AA98A9",
		},
		"& .MuiTableCell-head": {
			fontSize: "13px",
			fontWeight: "bold",
		},
		"& .MuiTableCell-body": {
			fontSize: "13px",
		},
	},
	tableRowWrap: {
		position: "relative",
	},
	tableCellWrap: {
		borderBottom: 0,
	},
	noResult: {
		position: "absolute",
		display: "block",
		fontSize: "14px",
		textAlign: "center",
		width: "100%",
	},
}));
