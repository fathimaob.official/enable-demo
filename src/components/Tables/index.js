import React from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import clsx from "clsx";
import { isEmpty } from "lodash";
import PropTypes from "prop-types";
import { useStyles } from "./styles";

/**
 * Table
 * @param {Array} columns Details of table columns to be rendered
 * @param {Array} data Array data to render
 */
const Tables = ({ className, columns, data }) => {
	const classes = useStyles();
	return (
		<TableContainer className={clsx(classes.tableContainer, className)} component={Paper}>
			<Table className={classes.table}>
				<TableHead>
					<TableRow>
						{columns.map((item, index) => (
							<TableCell key={`TABLE_HEAD_${index}`}>{item.label}</TableCell>
						))}
					</TableRow>
				</TableHead>
				<TableBody>
					{!isEmpty(data) ? (
						data.map((row, index) => (
							<TableRow key={`TABLE_ROW_${index}`}>
								{columns.map((item, index) => (
									<TableCell key={`TABLE_CELL_${index}`}>
										{item.render ? item.render(row) : row[item.accessor]}
									</TableCell>
								))}
							</TableRow>
						))
					) : (
						<TableRow className={classes.tableRowWrap}>
							<TableCell className={classes.tableCellWrap}>
								<p className={classes.noResult}>No results found.</p>
							</TableCell>
						</TableRow>
					)}
				</TableBody>
			</Table>
		</TableContainer>
	);
};

Tables.propTypes = {
	className: PropTypes.any,
	columns: PropTypes.array.isRequired,
	data: PropTypes.array.isRequired,
};

export default Tables;
