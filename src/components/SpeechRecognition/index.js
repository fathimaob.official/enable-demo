import React from "react";
import SpeechRecognition, { useSpeechRecognition } from "react-speech-recognition";
import { Drawer, Tooltip } from "@material-ui/core";
import MicIcon from "@material-ui/icons/Mic";
import SettingsVoiceIcon from "@material-ui/icons/SettingsVoice";
import SearchIcon from "@material-ui/icons/Search";
import CancelIcon from "@material-ui/icons/Cancel";
import PropTypes from "prop-types";
import "./styles.scss";

/**
 * Speech Recognition
 * For voice over search
 */
const SpeechRecognitioning = ({ isOpen, onClose, initiateSearch }) => {
	const { transcript, listening, resetTranscript, browserSupportsSpeechRecognition } = useSpeechRecognition();

	if (!browserSupportsSpeechRecognition) {
		return <span>Browser doesn't support speech recognition.</span>;
	}

	// Start listening
	const startListening = () => SpeechRecognition.startListening({ continuous: true, language: "en-US" });

	// Search Click
	const dosearch = () => {
		SpeechRecognition.stopListening();
		resetTranscript();
		initiateSearch(transcript);
		handleClose();
	};

	// Close modal
	const handleClose = () => {
		resetTranscript();
		onClose();
	};

	// eslint-disable-next-line no-console,
	console.log(" : ", transcript);

	return (
		<Drawer anchor="top" open={isOpen} onClose={handleClose} className="speechDrawer">
			<CancelIcon className="closeIcon" onClick={handleClose} />
			<div className="speechwrapper">
				{/* Text */}
				<p className="speech-text">
					{transcript ? transcript : listening ? "Listening..." : "Click to start listening"}
				</p>

				{/* Mic */}
				{listening ? (
					<Tooltip title="Click to stop listening">
						<div className="mic-icon-wrap speech-icon-wrap" onClick={SpeechRecognition.stopListening}>
							<SettingsVoiceIcon className="mic-icon" />
						</div>
					</Tooltip>
				) : (
					<Tooltip title="Click to start to listening">
						<div className="mic-icon-wrap" onClick={startListening}>
							<MicIcon className="mic-icon" />
						</div>
					</Tooltip>
				)}

				{/* Search */}
				<Tooltip title="Search">
					<div className="mic-icon-wrap" onClick={dosearch}>
						<SearchIcon className="mic-icon" />
					</div>
				</Tooltip>
			</div>
		</Drawer>
	);
};

SpeechRecognitioning.propTypes = {
	isOpen: PropTypes.bool.isRequired,
	onClose: PropTypes.func.isRequired,
	initiateSearch: PropTypes.func.isRequired,
};

export default SpeechRecognitioning;
