import React, { useState } from "react";
import MicIcon from "@material-ui/icons/Mic";
import { makeStyles } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import InputBase from "@material-ui/core/InputBase";
import PropTypes from "prop-types";
import SpeechRecognitioning from "../SpeechRecognition";

/**
 * Search Bar
 * Supports Key type search and Voice Recognition Search
 */
const Search = ({ searchKey, doSearch }) => {
	const classes = useStyles();
	const [isOpenSpeechModal, setIsOpenSpeechModal] = useState(false);
	return (
		<div className={classes.search}>
			<div className={classes.searchIcon}>
				<SearchIcon />
			</div>
			<InputBase
				placeholder="Search…"
				value={searchKey}
				classes={{
					root: classes.inputRoot,
					input: classes.inputInput,
				}}
				inputProps={{ "aria-label": "search" }}
				onChange={(e) => doSearch(e?.target?.value || "")}
			/>
			<div className={classes.searchEndIcon}>
				<MicIcon onClick={() => setIsOpenSpeechModal(true)} />
			</div>

			{/* Ṣpeech Recognition Component */}
			{/* Mounted each time to fix resetting transcript */}
			{isOpenSpeechModal && (
				<SpeechRecognitioning
					isOpen={isOpenSpeechModal}
					onClose={() => setIsOpenSpeechModal(false)}
					initiateSearch={(key) => doSearch(key)}
				/>
			)}
		</div>
	);
};

Search.propTypes = {
	searchKey: PropTypes.string.isRequired,
	doSearch: PropTypes.func.isRequired,
};

const useStyles = makeStyles((theme) => ({
	search: {
		position: "relative",
		display: "flex",
		alignItems: "center",
		borderRadius: "10rem",
		backgroundColor: "#FFFFFF",
		border: "2px solid #AA98A9",
		marginLeft: 0,
		width: "50%",
		maxWidth: "40rem",
		boxShadow: "0px 0px 7px -2px #aa98a9",
	},
	searchIcon: {
		padding: theme.spacing(0, 2),
		position: "absolute",
		pointerEvents: "none",
		display: "flex",
		alignItems: "center",
		color: "#AA98A9",
	},
	searchEndIcon: {
		position: "relative",
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		color: "#AA98A9",
		paddingRight: "9px",
		right: 0,
		cursor: "pointer",
		"&:hover": {
			color: "#6c616b",
		},
	},
	inputRoot: {
		color: "inherit",
		width: "100%",
	},
	inputInput: {
		padding: theme.spacing(1, 1, 1, 0),
		// vertical padding + font size from searchIcon
		paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
		width: "100%",
		fontSize: "14px",
	},
}));

export default Search;
