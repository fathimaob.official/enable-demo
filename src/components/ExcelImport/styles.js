import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
	importDrawer: {
		position: "relative",
		"& .MuiDrawer-paper ": { minHeight: "9rem" },
	},
	importWrapper: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		padding: "1rem",
	},
	alertTitle: {
		marginBottom: "6px",
	},
	alertSmall: {
		fontSize: "12px",
		marginBottom: "5px",
	},
	errorTable: {
		width: "80%",
		maxHeight: "18rem",
		marginTop: "2rem",
	},
	closeIcon: {
		position: "absolute",
		right: 10,
		top: 5,
		color: "#AA98A9",
		cursor: "pointer",
	},
}));
export default useStyles;
