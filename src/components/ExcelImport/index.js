import { Drawer } from "@material-ui/core";
import React, { useState } from "react";
import readXlsxFile from "read-excel-file";
import PropTypes from "prop-types";
import CancelIcon from "@material-ui/icons/Cancel";
import { email } from "../../util/formValidator";
import excelSample from "../../assets/images/excelSample.PNG";
import useStyles from "./styles";
import { isEmpty } from "lodash";
import Tables from "../Tables";

/**
 * Popover to Import Excel File
 */
const ExcelImport = ({ isOpen, handleClose, addUsers }) => {
	const classes = useStyles();
	const [rowUpdates, setRowUpdates] = useState(null);

	// Handle the file import
	const changeHandler = (event) => {
		setRowUpdates(null);
		readXlsxFile(event.target.files[0], { schema }).then(({ rows, errors }) => {
			if (isEmpty(errors)) {
				addUsers(rows);
				handleClose();
			} else {
				setRowUpdates({ rows, errors });
			}
		});
	};
	// Schema to validate the excel sheet
	const schema = {
		NAME: {
			prop: "name",
			type: String,
			required: true,
		},
		EMAIL: {
			prop: "email",
			type: (value) => {
				const number = email(value);
				console.log("b : ", { value, number });
				if (number) {
					throw new Error(number);
				}
				return value;
			},
			required: true,
		},
		ADDRESS: {
			prop: "address",
			type: {
				STREET: {
					prop: "street",
					type: String,
				},
				SUITE: {
					prop: "suite",
					type: String,
				},
				CITY: {
					prop: "city",
					type: String,
					required: true,
				},
				ZIPCODE: {
					prop: "zipcode",
					type: String,
				},
			},
		},
		COMPANY: {
			prop: "company",
			type: {
				NAME: {
					prop: "name",
					type: String,
					required: true,
				},
			},
		},
	};

	// Error list table
	const columns = [
		{
			label: "Row",
			accessor: "row",
		},
		{
			label: "Column",
			accessor: "column",
		},
		{
			label: "Error",
			accessor: "error",
		},
	];

	// Close modal
	const onClose = () => {
		setRowUpdates(null);
		handleClose();
	};

	return (
		<Drawer anchor="top" open={isOpen} onClose={onClose} className={classes.importDrawer}>
			<CancelIcon className={classes.closeIcon} onClick={onClose} />
			<div className={classes.importWrapper}>
				{/* Import */}
				<h3>Upload Excel File</h3>
				<input
					type="file"
					name="file"
					accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
					onChange={changeHandler}
				/>

				{/* Sample Info */}
				<h4 className={classes.alertTitle}>Sample Sheet</h4>
				<span className={classes.alertSmall}>* Please note: Name, Email, City & Company Name are mandatory</span>
				<img src={excelSample} alt="Excel Sample" />

				{/* Error Table */}
				{rowUpdates && !isEmpty(rowUpdates.errors) && (
					<Tables className={classes.errorTable} columns={columns} data={rowUpdates.errors} />
				)}
			</div>
		</Drawer>
	);
};

ExcelImport.propTypes = {
	isOpen: PropTypes.bool.isRequired,
	handleClose: PropTypes.func.isRequired,
	addUsers: PropTypes.func.isRequired,
};

export default ExcelImport;
