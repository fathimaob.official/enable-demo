import React, { forwardRef } from "react";
import { Button, Dialog, DialogContent, DialogActions, Slide, DialogTitle } from "@material-ui/core";
import clsx from "clsx";
import PropTypes from "prop-types";
import useStyles from "./styles";

const Transition = forwardRef(function Transition(props, ref) {
	return <Slide direction="up" ref={ref} {...props} />;
});

/**
 * Delete Confirmation Dialog
 */
const ConfirmDialog = ({ isOpen = false, onClose, onYes }) => {
	const classes = useStyles();

	return (
		<Dialog
			open={isOpen}
			className={classes.confirmDialogPopup}
			TransitionComponent={Transition}
			keepMounted
			onClose={onClose}>
			<DialogTitle id="alert-dialog-slide-title" className={classes.confirmDialogTitle}>
				<span className={classes.confirmDialogText}>Confirm Delete</span>
			</DialogTitle>

			<DialogContent>
				<p className={classes.confirmDialogBody}>Are you sure you want to delete?</p>
			</DialogContent>
			<DialogActions>
				<Button className={clsx(classes.btn, classes.btnSecondary)} onClick={onClose}>
					Cancel
				</Button>
				<Button className={clsx(classes.btn, classes.btnPrimary)} onClick={onYes} variant="contained">
					Delete
				</Button>
			</DialogActions>
		</Dialog>
	);
};

ConfirmDialog.propTypes = {
	isOpen: PropTypes.bool.isRequired,
	onClose: PropTypes.func.isRequired,
	onYes: PropTypes.func.isRequired,
};

export default ConfirmDialog;
