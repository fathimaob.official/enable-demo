import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
	confirmDialogPopup: {
		"& .MuiDialog-paper": {
			width: "400px",
		},
		"& .MuiDialogActions-spacing": {
			marginBottom: theme.spacing(2),
			marginRight: theme.spacing(2),
		},
	},
	confirmDialogTitle: {
		paddingBottom: "5px",
	},
	confirmDialogText: {
		fontWeight: "600",
	},
	confirmDialogBody: {
		fontSize: "14px",
		marginBottom: "1rem",
	},
	btn: {
		textTransform: "capitalize",
		marginLeft: "1rem",
	},
	btnSecondary: { color: "#6c616b" },
	btnPrimary: {
		backgroundColor: "#AA98A9",
		color: "#FFF",
	},
}));

export default useStyles;
