import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core";

/**
 * Wrapper around the pages
 */
const Layout = ({ children }) => {
	const classes = useLayoutStyles();
	return <div className={classes.layoutWrapper}>{children}</div>;
};

Layout.propTypes = {
	children: PropTypes.any.isRequired,
};

const useLayoutStyles = makeStyles((theme) => ({
	layoutWrapper: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "center",
		height: "100%",
		backgroundColor: "#fafafa",
	},
}));

export default Layout;
