import { USERS } from "../types";

// Get users list
export const getUsersList = () => {
	return (dispatch, getState) => {
		dispatch({
			type: USERS.GET_USERS,
		});
	};
};

// Search user by name
export const searchUsers = (key = "") => {
	return (dispatch, getState) => {
		dispatch({
			type: USERS.SEARCH_USERS,
			key,
		});
	};
};

// Add user
export const addUser = (body) => {
	return (dispatch, getState) => {
		dispatch({
			type: USERS.ADD_USER,
			body,
		});
	};
};

// Add bulk users (Excel import)
export const addUsersBulk = (body) => {
	return (dispatch, getState) => {
		dispatch({
			type: USERS.ADD_USERS_BULK,
			body,
		});
	};
};

// Edit user
export const editUser = (body) => {
	return (dispatch, getState) => {
		dispatch({
			type: USERS.EDIT_USER,
			body,
		});
	};
};

// Delete user
export const deleteUser = (key) => {
	return (dispatch, getState) => {
		dispatch({
			type: USERS.DELETE_USER,
			key,
		});
	};
};
