import { combineReducers } from "redux";

import Users from "./usersReducer";

export default combineReducers({ Users });
