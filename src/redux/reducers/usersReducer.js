import { filter, isEmpty, map } from "lodash";
import { USERS } from "../types";
import { sortAlpha, setUser, getUser, searchByName } from "../../util/util";
import userJSON from "../../util/users.json";

const initialState = {
	searchKey: "",
	usersList: [],
};

const usersReducer = (state = initialState, action) => {
	const savedUsers = getUser();
	switch (action.type) {
		// List users
		case USERS.GET_USERS:
			const sortedArr = sortAlpha(userJSON);
			const usersArr = !isEmpty(savedUsers) ? savedUsers : sortedArr;
			setUser(usersArr);
			return {
				...initialState,
				usersList: usersArr,
			};

		// Search users by name
		case USERS.SEARCH_USERS:
			const key = action.key || "";
			const users = searchByName(savedUsers, key);
			return {
				...state,
				searchKey: key,
				usersList: users,
			};

		// Create new user
		case USERS.ADD_USER:
			const newUser = action.body;
			const addedList = [newUser, ...savedUsers];
			const sortAddedList = sortAlpha(addedList);
			setUser(sortAddedList);
			return {
				...state,
				usersList: searchByName(sortAddedList, state.searchKey),
			};

		// Create users bulk
		case USERS.ADD_USERS_BULK:
			const newUsers = action.body;
			const addedLists = [...newUsers, ...savedUsers];
			const sortAddedLists = sortAlpha(addedLists);
			setUser(sortAddedLists);
			return {
				...state,
				usersList: searchByName(sortAddedLists, state.searchKey),
			};

		// Edit user
		case USERS.EDIT_USER:
			const editedUser = action.body;
			const newEditedList = map(savedUsers, (o) => {
				if (o.id === editedUser.id) return editedUser;
				else return o;
			});
			const sortEditedList = sortAlpha(newEditedList);
			setUser(sortEditedList);
			return {
				...state,
				usersList: searchByName(sortEditedList, state.searchKey),
			};

		// Delete user by id
		case USERS.DELETE_USER:
			const deleteId = action.key;
			const deletedList = filter(savedUsers, (o) => o.id !== deleteId);
			const sortDeletedList = sortAlpha(deletedList);
			setUser(sortDeletedList);
			return {
				...state,
				usersList: searchByName(sortDeletedList, state.searchKey),
			};

		default:
			return state;
	}
};
export default usersReducer;
