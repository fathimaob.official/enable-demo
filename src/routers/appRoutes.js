import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import Users from "../pages/users";

const AppRoutes = () => (
	<Router>
		<Switch>
			<Route exact path="/" component={Users} />
			<Redirect to="/" />
		</Switch>
	</Router>
);
export default AppRoutes;
